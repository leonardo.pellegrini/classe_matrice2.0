import javax.swing.plaf.synth.SynthSpinnerUI;
import javax.swing.text.MaskFormatter;

public class App {
    public static void main(String[] args) throws Exception {

        /*//creazione di una matrice double
        double [][] m ={{1,2,3},{4,5,6},{3,1,1}};

        //creazione oggetto Matrice con costruttore che passa una matrice di double
        Matrice A = new Matrice(m);

        //metodo addizione
        Matrice C = A.addizione(A); //C = A+A
        System.out.println(A);
        System.out.println(C);

        /*
        Matrice f1 = new Matrice(b); //questa è una deep copy
        Matrice f2 = b; //questa è una shallow copy
        */
    
        //System.out.println(new Matrice(m).addizione(b)); //method chains

        /*Matrice MatriceIdentity = Matrice.creaMatriceIdentity(5);
        System.out.println(MatriceIdentity);
        System.out.println(MatriceIdentity.isIdentity());*/

        double [][] m = {{1,1,1},{1,1,1},{1,1,1}};
        double [][] m2 = {{2,2,2},{2,2,2},{2,2,2}};
        double [][] m3 = {{3,3,3},{3,3,3},{3,3,3}};

        Matrice A = new Matrice(m);
        Matrice B = new Matrice(m2);
        Matrice C = new Matrice(m3);


        //proprietà associativa
       
        boolean isAssociativa = (A.prodotto(B.prodotto(C))).equals(C.prodotto(A.prodotto(B)));

        System.out.println(isAssociativa);
        
 

        //proprietà distributiva
        
        Boolean isDistributiva = (A.prodotto(B.addizione(C))).equals((A.prodotto(B).addizione(A.prodotto(C))));
        
        System.out.println(isDistributiva);

        


        //proprità matrice nulla
        double [][] n = {{0,0,0},{0,0,0},{0,0,0}};
        Matrice nulla = new Matrice(n);

        Matrice prodottoNullo = A.prodotto(nulla);
        System.out.println(prodottoNullo);

        //anche se nessuna delle due è nulla può risultare una matrice prodotto nulla
        double [][] p = {{3,6},{1,2}};
        double [][] p2 = {{0,2},{0,-1}};

        Matrice R = new Matrice(p);
        Matrice R2 = new Matrice(p2);

        Matrice F = R.prodotto(R2);
        System.out.println(F);

        
        
        Matrice prodottoIA = (Matrice.creaMatriceIdentity(3)).prodotto(A);
        System.out.println(A.equals(prodottoIA));

    }
}
