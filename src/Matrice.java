public class Matrice {
    private double [][] matrice; //in java una matrice è un array di array

    //costruttori
    public Matrice() {
    }

    public Matrice(int nRighe, int nColonne) { 
        matrice = new double[nRighe][nColonne];
    }

    public Matrice(double [][] m){ 
        matrice = new double [m.length][m[0].length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                matrice[i][j] = m[i][j];
            }
        }
        
    }

    public Matrice(Matrice m){ //costruttore di copia 
        for (int i = 0; i < m.matrice.length; i++) {
            for (int j = 0; j < m.matrice[0].length; j++) {
                matrice[i][j] = m.matrice[i][j];
            }
        }
    }



    //metodi set e get
    public boolean setItem(int r, int c, double num){
        boolean ris = false;
        if(r >= 0 && r <= matrice.length && c >= 0 && c <= matrice[0].length){
            matrice[r][c] = num;
            ris = true;
        }
        return ris;
    }

    public double getItem(int r, int c){
        return matrice[r][c];
    }

    //metodo per addizionare due matrici
    public Matrice addizione(Matrice B){
        Matrice C = null;

        if(matrice.length == B.getNRighe() && matrice[0].length == B.getNColonne()){ //controllo se le matrici hanno le stesse dimensioni

            C = new Matrice(matrice.length, matrice[0].length); 

            for (int i = 0; i < matrice.length; i++) {
                for (int j = 0; j < matrice[0].length; j++) {

                    C.setItem(i, j, matrice[i][j] + B.getItem(i, j)); 
                    //C.matrice[i][j] = matrice[i][j] + B.matrice[i][j]; possiamo farlo perche siamo nella classe matrice
                }
            }
        }
        return C;
    }


    
    //metodo per moltiplicare i valori con un'altra matrice
    public Matrice prodotto(Matrice m){
        Matrice matriceProdotto = null;

        if(matrice.length == m.getNColonne() && matrice[0].length == m.getNRighe()){ //controllo se le dimensioni sono corrette
            
            matriceProdotto = new Matrice(matrice.length, m.getNColonne());      
            double somma = 0;   

            for(int i = 0; i<matriceProdotto.matrice.length; i++){ 

                for(int j = 0; j<matriceProdotto.matrice[0].length; j++){

                    somma = 0;
                    for(int x = 0; x<this.getNColonne(); x++){
                    
                        somma += matrice[i][x]*m.getItem(x, j); 
                            
                    } 
                     
                    matriceProdotto.setItem(i, j, somma);     
                } 
          
            }
        }
        return matriceProdotto;
    }



    //getters e setters
    public int getNRighe() {
        return matrice.length;
    }

    public int getNColonne() {
        return matrice[0].length;
    }

    public double[][] getMatrice() {
        return this.matrice;
    }

    public void setMatrice(double[][] matrice) {
        this.matrice = matrice;
    }

    //metodo che crea una matrice idenità
    public static Matrice creaMatriceIdentity(int dimensione){
        Matrice matriceIdentity = new Matrice(dimensione, dimensione);

        for(int i = 0; i < dimensione; i++){
            for(int j = 0; j < dimensione; j++){
                matriceIdentity.setItem(i, j, 0);
            }
            matriceIdentity.setItem(i, i, 1);
        }

        return matriceIdentity;
    }

    //metodo isIdentity
    /*public Boolean isIdentity(){
        Boolean a = false;
        if(matrice.length == matrice[0].length){
            a = true;
            for(int i = 0; i<matrice.length; i++){
                for(int j = 0; j<matrice.length; j++){
                    if((i==j && matrice[i][i] != 1) || (i!=j && matrice[i][j] != 0)){
                        a = false;
                        return a;
                    }
                }
            }
        }
        return a;
        
    }*/

    public Boolean isIdentity(){
        Boolean a = false;
        if(matrice.length == matrice[0].length){
            a = true;
            int i = 0, j = 0;
            
            while(a && i < getNRighe()){
                j = 0;
                while(a && j < getNColonne() ){
                    if((i==j && matrice[i][i] != 1) || (i!=j && matrice[i][j] != 0)){
                        a = false;
                        
                    }
                    j++;
                }

                i++;
                if(i == getNRighe()){
                    i++; //per uscire dal while
                }

                
            }
        }
        return a;
        
    }
    


    public Matrice trasposta(){
        Matrice MatriceTraspostata = new Matrice(getNColonne(), getNRighe());
        for(int i = 0; i<getNColonne(); i++){
            for(int j = 0; j<getNRighe(); j++){
                MatriceTraspostata.setItem(j, i, matrice[i][j]);
            }
        }
        return MatriceTraspostata;
    }

    //metodo equals
    public boolean equals(Matrice m) {
        boolean ris = false;

        if(this.getNColonne() == m.getNColonne() && getNRighe() == m.getNRighe()){
            ris = true;
            for(int i = 0; i<getNRighe();i++){
                for(int j = 0; j< getNColonne(); j++){
                    if(matrice[i][j] != m.matrice[i][j]){
                        ris = false;
                    }
                }
            }
        }


        return ris;
    }

    //metodo toString
    public String toString() {
        String x = System.lineSeparator();

        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice[0].length; j++) {
                //x += String.format("%6.2f", matrice[i][j] + " ");
                x += matrice[i][j] + "  ";
            }
            x += System.lineSeparator();
        }
        return  
             x 
            ;
    }
}
